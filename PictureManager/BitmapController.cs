﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PictureManager
{
    public class BitmapController
    {
        private Image imageToConvert;

        public BitmapController(Image imageToConvert)
        {
            this.imageToConvert = imageToConvert;
        }

        public Bitmap GetBitMap()
        {
            Bitmap resultBmp = new Bitmap(this.imageToConvert);

            return resultBmp;
        }

        public Image GetImageToConvert
        {
            get
            {
                if (this.imageToConvert == null)
                {
                    throw new ArgumentNullException(String.Format("The image you're tring to convert is not available!"));
                }
                return this.imageToConvert;
            }
        }
    }
}

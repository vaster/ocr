﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;

namespace PictureManager
{
    public class ImageController
    {
        private byte[] imgAsBytes;


        public ImageController(byte[] imageAsBytes)
        {
            this.imgAsBytes = imageAsBytes;
        }

        public Image GetImage()
        {
            MemoryStream memoryStream = new MemoryStream(this.GetImageBytes);
            Image resultImage = Image.FromStream(memoryStream);

            return resultImage;
        }

        public byte[] GetImageBytes
        {
            get
            {
                if (imgAsBytes == null || imgAsBytes.Length == 0)
                {
                    throw new ArgumentNullException(String.Format("No image is uloaded to proceed work!"));
                }
                return this.imgAsBytes;
            }
        }
    }
}

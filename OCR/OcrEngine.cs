﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Drawing;
using Emgu.CV.OCR;
using Emgu.CV;
using Emgu.CV.Structure;

namespace OCR
{
    public class OcrEngine
    {
        private const int CHAR_ON_LINE = 65;
        private const int LINES_ON_PAGE = 24;

        private const string TESSERACT_LANG_DATA_PATH = "tessdata";

        private Tesseract ocr;

        // possible: language to be optional. Some automated logic to determ it.
        public OcrEngine(string rootPath, string langType)
        {
            this.ocr = new Tesseract(rootPath + OcrEngine.TESSERACT_LANG_DATA_PATH,
               langType, Tesseract.OcrEngineMode.OEM_TESSERACT_ONLY);
        }

        private string NormilizeForHtml(string txt)
        {
            //string[] replaced = txt.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
            string[] replaced = txt.Split(new string[] { "\r\n" }, int.MaxValue, StringSplitOptions.RemoveEmptyEntries);

            // string tmpResult = String.Join("", replaced);
            StringBuilder result = new StringBuilder();

            foreach (var item in replaced)
            {
                result.Append(item + "\n");
            }

            //int lineCount = 0;
            //for (int i = 0; i < tmpResult.Length; i++)
            //{
            //    if (i % OcrEngine.CHAR_ON_LINE == 0)
            //    {
            //        result.Append(" </br> ");
            //        lineCount++;
            //    }
            //    else
            //    {
            //        result.Append(tmpResult[i]);
            //    }
            //    if (lineCount % OcrEngine.LINES_ON_PAGE == 0)
            //    {
            //        result.Append("~");
            //        lineCount = 1;
            //    }
            //}

            return result.ToString();
        }


        public string GetText(Bitmap bitmap)
        {
            Image<Gray, Byte> emguImage = new Image<Gray, byte>(bitmap);

            this.ocr.Recognize(emguImage);
            string txt = ocr.GetText();
            string result = this.NormilizeForHtml(txt);

            return @result;
        }
    }
}

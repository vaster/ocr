﻿using DocumentManager;
using OCR;
using PictureManager;
using Root.Reports;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Text;
using System.Web;
using System.Web.Http;


namespace Ocr.WebApi.Controllers
{
    public class OcrController : ApiController
    {
        [DllImport("kernel32.dll")]
        public static extern IntPtr LoadLibrary(string dllToLoad);

        private ImageController imgController;
        private BitmapController bitmapController;
        private OcrEngine ocr;

        public OcrController()
        {
            //this.textFromImg = null;
            this.imgController = null;
            this.ocr = null;
            this.bitmapController = null;
        }

        public HttpResponseMessage PostText(string lang, string typeExt)
        {
            StringBuilder error = new StringBuilder("no err");
            //try
            {
                string rootPath = HttpContext.Current.Request.PhysicalApplicationPath;

                IntPtr pDll1_1 = LoadLibrary(rootPath + @"lib\cudart32_50_35.dll");
                if (pDll1_1 == IntPtr.Zero)
                {
                    throw new NotImplementedException("cudart32_50_35.dll");
                }

                IntPtr pDll1_2 = LoadLibrary(rootPath + @"lib\npp32_50_35.dll");
                if (pDll1_2 == IntPtr.Zero)
                {
                    throw new NotImplementedException("npp32_50_35.dll");
                }

                IntPtr pDll1 = LoadLibrary(rootPath + @"lib\opencv_core249.dll");
                if (pDll1 == IntPtr.Zero)
                {
                    throw new NotImplementedException("opencv_core249.dll");
                }

                IntPtr pDll4_1 = LoadLibrary(rootPath + @"lib\opencv_imgproc249.dll");
                if (pDll4_1 == IntPtr.Zero)
                {
                    throw new NotImplementedException("opencv_imgproc249.dll");
                }

                IntPtr pDll4_2 = LoadLibrary(rootPath + @"lib\opencv_flann249.dll");
                if (pDll4_2 == IntPtr.Zero)
                {
                    throw new NotImplementedException("opencv_flann249.dll");
                }

                IntPtr pDll4_3 = LoadLibrary(rootPath + @"lib\opencv_features2d249.dll");
                if (pDll4_3 == IntPtr.Zero)
                {
                    throw new NotImplementedException("opencv_features2d249.dll");
                }

                IntPtr pDll4_4 = LoadLibrary(rootPath + @"lib\opencv_calib3d249.dll");
                if (pDll4_4 == IntPtr.Zero)
                {
                    throw new NotImplementedException("opencv_calib3d249.dll");
                }

                IntPtr pDll4_5_1 = LoadLibrary(rootPath + @"lib\opencv_highgui249.dll");
                if (pDll4_5_1 == IntPtr.Zero)
                {
                    throw new NotImplementedException("opencv_highgui249.dll");
                }

                IntPtr pDll4_5 = LoadLibrary(rootPath + @"lib\opencv_objdetect249.dll");
                if (pDll4_5 == IntPtr.Zero)
                {
                    throw new NotImplementedException("opencv_objdetect249.dll");
                }

                IntPtr pDll4_6 = LoadLibrary(rootPath + @"lib\opencv_video249.dll");
                if (pDll4_6 == IntPtr.Zero)
                {
                    throw new NotImplementedException("opencv_video249.dll");
                }

                IntPtr pDll4_7 = LoadLibrary(rootPath + @"lib\cudart32_50_35.dll");
                if (pDll4_7 == IntPtr.Zero)
                {
                    throw new NotImplementedException("cudart32_50_35.dll");
                }

                IntPtr pDll4_8 = LoadLibrary(rootPath + @"lib\npp32_50_35.dll");
                if (pDll4_8 == IntPtr.Zero)
                {
                    throw new NotImplementedException("npp32_50_35.dll");
                }

                IntPtr pDll4_9 = LoadLibrary(rootPath + @"lib\cufft32_50_35.dll");
                if (pDll4_9 == IntPtr.Zero)
                {
                    throw new NotImplementedException("cufft32_50_35.dll");
                }

                IntPtr pDll4_10 = LoadLibrary(rootPath + @"lib\cublas32_50_35.dll");
                if (pDll4_10 == IntPtr.Zero)
                {
                    throw new NotImplementedException("cublas32_50_35.dll");
                }

                IntPtr pDll4 = LoadLibrary(rootPath + @"lib\opencv_gpu249.dll");
                if (pDll4_10 == IntPtr.Zero)
                {
                    throw new NotImplementedException("opencv_gpu249.dll");
                }

                IntPtr pDll5_1 = LoadLibrary(rootPath + @"lib\opencv_ml249.dll");
                if (pDll5_1 == IntPtr.Zero)
                {
                    throw new NotImplementedException("opencv_ml249.dll");
                }

                IntPtr pDll5 = LoadLibrary(rootPath + @"lib\opencv_contrib249.dll");
                if (pDll5 == IntPtr.Zero)
                {
                    throw new NotImplementedException("opencv_contrib249.dll");
                }

                IntPtr pDll6 = LoadLibrary(rootPath + @"lib\opencv_legacy249.dll");
                if (pDll6 == IntPtr.Zero)
                {
                    throw new NotImplementedException("opencv_legacy249.dll");
                }

                IntPtr pDll7 = LoadLibrary(rootPath + @"lib\opencv_nonfree249.dll");
                if (pDll7 == IntPtr.Zero)
                {
                    throw new NotImplementedException("opencv_nonfree249.dll");
                }

                IntPtr pDll8 = LoadLibrary(rootPath + @"lib\opencv_stitching249.dll");
                if (pDll8 == IntPtr.Zero)
                {
                    throw new NotImplementedException("opencv_stitching249.dll");
                }

                IntPtr pDll9 = LoadLibrary(rootPath + @"lib\opencv_photo249.dll");
                if (pDll9 == IntPtr.Zero)
                {
                    throw new NotImplementedException("opencv_photo249.dll");
                }

                IntPtr pDll10 = LoadLibrary(rootPath + @"lib\opencv_videostab249.dll");
                if (pDll10 == IntPtr.Zero)
                {
                    throw new NotImplementedException("opencv_videostab249.dll");
                }

                IntPtr pDll11 = LoadLibrary(rootPath + @"lib\cvextern.dll");
                if (pDll11 == IntPtr.Zero)
                {
                    throw new NotImplementedException("cvextern.dll");
                }


                byte[] imgAsBytes = this.Request.Content.ReadAsByteArrayAsync().Result;

                imgController = new ImageController(imgAsBytes);

                this.ocr = new OcrEngine(rootPath, lang);

                Image imageToConvert = this.imgController.GetImage();

                this.bitmapController = new BitmapController(imageToConvert);

                Bitmap bitmapToReadFrom = this.bitmapController.GetBitMap();

                string txt = this.ocr.GetText(bitmapToReadFrom);

                return this.Request.CreateResponse(HttpStatusCode.Created, txt);

            }
            //catch (Exception ex)
            //{
            //    var currEx = ex;
            //    while (currEx != null)
            //    {
            //        error.AppendLine("msg:" + currEx.Message);
            //
            //        currEx = currEx.InnerException;
            //    }
            //
            //}
        }
    }
}

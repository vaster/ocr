﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace DocumentManager
{
    public class XmlParser
    {
        private string content;
        
        public XmlParser(string content)
        {
            this.content = content;
        }

        public XDocument Genererate()
        {
            XDocument resultDoc = new XDocument();
            resultDoc.Save(this.content);

            return resultDoc;
        }
    }
}

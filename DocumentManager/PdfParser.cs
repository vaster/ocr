﻿using PdfSharp.Pdf;
using Root.Reports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentManager
{
    public class PdfParser
    {
        private string content;

        public PdfParser(string content)
        {
            this.content = content;
        }

        public Report GetPdf()
        {
            Report pdf = new Report(new PdfFormatter());
            FontDef font = new FontDef(pdf, "Helvetica");
            FontProp size = new FontPropMM(font, 6);
            Page page = new Page(pdf);
            page.Add(0, 20, new RepString(size, this.content));

            return pdf;
        }
    }
}
